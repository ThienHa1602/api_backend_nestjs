/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE TABLE `binh_luan` (
  `binh_luan_id` int NOT NULL AUTO_INCREMENT,
  `ngay_binh_luan` datetime DEFAULT NULL,
  `noi_dung` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `nguoi_dung_id` int DEFAULT NULL,
  `hinh_id` int DEFAULT NULL,
  PRIMARY KEY (`binh_luan_id`),
  KEY `hinh_id` (`hinh_id`),
  KEY `nguoi_dung_id` (`nguoi_dung_id`),
  CONSTRAINT `binh_luan_ibfk_2` FOREIGN KEY (`hinh_id`) REFERENCES `hinh_anh` (`hinh_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `binh_luan_ibfk_3` FOREIGN KEY (`nguoi_dung_id`) REFERENCES `nguoi_dung` (`nguoi_dung_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `hinh_anh` (
  `hinh_id` int NOT NULL AUTO_INCREMENT,
  `ten_anh` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `duong_dan` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `mo_ta` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `nguoi_dung_id` int DEFAULT NULL,
  PRIMARY KEY (`hinh_id`),
  KEY `nguoi_dung_id` (`nguoi_dung_id`),
  CONSTRAINT `hinh_anh_ibfk_1` FOREIGN KEY (`nguoi_dung_id`) REFERENCES `nguoi_dung` (`nguoi_dung_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `luu_anh` (
  `nguoi_dung_id` int NOT NULL,
  `hinh_id` int NOT NULL,
  `ngay_luu` date DEFAULT NULL,
  PRIMARY KEY (`nguoi_dung_id`,`hinh_id`),
  KEY `hinh_id` (`hinh_id`),
  CONSTRAINT `luu_anh_ibfk_3` FOREIGN KEY (`nguoi_dung_id`) REFERENCES `nguoi_dung` (`nguoi_dung_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `luu_anh_ibfk_4` FOREIGN KEY (`hinh_id`) REFERENCES `hinh_anh` (`hinh_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `nguoi_dung` (
  `nguoi_dung_id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `mat_khau` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `ho_ten` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `tuoi` int DEFAULT NULL,
  `anh_dai_dien` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`nguoi_dung_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `binh_luan` (`binh_luan_id`, `ngay_binh_luan`, `noi_dung`, `nguoi_dung_id`, `hinh_id`) VALUES
(1, NULL, 'hay', 1, 1);
INSERT INTO `binh_luan` (`binh_luan_id`, `ngay_binh_luan`, `noi_dung`, `nguoi_dung_id`, `hinh_id`) VALUES
(3, NULL, 'hay', 6, 1);
INSERT INTO `binh_luan` (`binh_luan_id`, `ngay_binh_luan`, `noi_dung`, `nguoi_dung_id`, `hinh_id`) VALUES
(4, NULL, 'hay', 7, 1);
INSERT INTO `binh_luan` (`binh_luan_id`, `ngay_binh_luan`, `noi_dung`, `nguoi_dung_id`, `hinh_id`) VALUES
(6, NULL, 'hay', 5, 3),
(7, NULL, 'hay', 5, 3),
(9, NULL, 'hay', 5, 3),
(13, NULL, 'very hay', 8, 3),
(14, NULL, 'very hay', 8, 3),
(15, '2023-07-23 00:00:00', 'very hay', 8, 3),
(16, '2023-07-23 15:34:59', 'very hay', 8, 3),
(17, '2023-07-23 15:36:42', 'very gút', 8, 3),
(18, '2023-07-23 15:49:39', 'very gút', 8, 1),
(19, '2023-07-23 15:51:06', 'very gút', 8, 4),
(20, '2023-07-23 15:53:42', NULL, NULL, NULL),
(21, '2023-07-23 15:57:48', 'd', 1, NULL),
(23, '2023-07-23 20:10:51', 'very gút', 8, 4),
(24, '2023-07-24 20:54:42', 'very gút', 11, 4);

INSERT INTO `hinh_anh` (`hinh_id`, `ten_anh`, `duong_dan`, `mo_ta`, `nguoi_dung_id`) VALUES
(1, 'abc', 'bca', 'ac', 1);
INSERT INTO `hinh_anh` (`hinh_id`, `ten_anh`, `duong_dan`, `mo_ta`, `nguoi_dung_id`) VALUES
(3, 'con mèo', 'bca', 'ac', 1);
INSERT INTO `hinh_anh` (`hinh_id`, `ten_anh`, `duong_dan`, `mo_ta`, `nguoi_dung_id`) VALUES
(4, 'con mèo', 'bca', 'ac', 7);
INSERT INTO `hinh_anh` (`hinh_id`, `ten_anh`, `duong_dan`, `mo_ta`, `nguoi_dung_id`) VALUES
(7, 'con mèo', 'bca', 'ac', 7),
(12, 'chó', 'bca', 'ac', 3),
(14, 'file', 'C:\\Users\\pc\\Desktop\\Học Tập - Back End\\Galaxy-rest\\be_galarest_nestjs\\public\\img\\1690198835095flash.jpg', NULL, 8),
(15, 'file', 'C:\\Users\\pc\\Desktop\\Học Tập - Back End\\Galaxy-rest\\be_galarest_nestjs\\public\\img\\1690199144453flash.jpg', NULL, 7),
(16, 'file', 'C:\\Users\\pc\\Desktop\\Học Tập - Back End\\Galaxy-rest\\be_galarest_nestjs\\public\\img\\1690199213064flash.jpg', NULL, 8),
(17, '1690199264418flash.jpg', 'C:\\Users\\pc\\Desktop\\Học Tập - Back End\\Galaxy-rest\\be_galarest_nestjs\\public\\img\\1690199264418flash.jpg', NULL, 9),
(18, '1690199359692flash.jpg', 'C:\\Users\\pc\\Desktop\\Học Tập - Back End\\Galaxy-rest\\be_galarest_nestjs\\public\\img\\1690199359692flash.jpg', NULL, 8),
(19, '1690199694308flash.jpg', 'C:\\Users\\pc\\Desktop\\Học Tập - Back End\\Galaxy-rest\\be_galarest_nestjs\\public\\img\\1690199694308flash.jpg', NULL, 8),
(20, '1690199996804flash.jpg', 'C:\\Users\\pc\\Desktop\\Học Tập - Back End\\Galaxy-rest\\be_galarest_nestjs\\public\\img\\1690199996804flash.jpg', 'the flash chay nhanh', 6),
(21, '1690200079933flash.jpg', 'C:\\Users\\pc\\Desktop\\Học Tập - Back End\\Galaxy-rest\\be_galarest_nestjs\\public\\img\\1690200079933flash.jpg', NULL, 8);

INSERT INTO `luu_anh` (`nguoi_dung_id`, `hinh_id`, `ngay_luu`) VALUES
(1, 1, NULL);
INSERT INTO `luu_anh` (`nguoi_dung_id`, `hinh_id`, `ngay_luu`) VALUES
(1, 3, NULL);
INSERT INTO `luu_anh` (`nguoi_dung_id`, `hinh_id`, `ngay_luu`) VALUES
(7, 1, NULL);
INSERT INTO `luu_anh` (`nguoi_dung_id`, `hinh_id`, `ngay_luu`) VALUES
(7, 3, NULL);

INSERT INTO `nguoi_dung` (`nguoi_dung_id`, `email`, `mat_khau`, `ho_ten`, `tuoi`, `anh_dai_dien`) VALUES
(1, 'kan@gmail.com', '123456', 'Kan', 25, NULL);
INSERT INTO `nguoi_dung` (`nguoi_dung_id`, `email`, `mat_khau`, `ho_ten`, `tuoi`, `anh_dai_dien`) VALUES
(2, 'embe@gmail.com', '123456', 'Thien Ha Ne', 21, 'C:\\Users\\pc\\Desktop\\Học Tập - Back End\\Galaxy-rest\\be_galarest_nestjs\\public\\avarta\\1690231223403New Sản Phẩm (1).png');
INSERT INTO `nguoi_dung` (`nguoi_dung_id`, `email`, `mat_khau`, `ho_ten`, `tuoi`, `anh_dai_dien`) VALUES
(3, 'kakaka@gmail.com', '123456', 'Kan', 25, 'C:\\Users\\pc\\Desktop\\Học Tập - Back End\\Galaxy-rest\\be_galarest_nestjs\\public\\avarta\\16902315198561234.jpg');
INSERT INTO `nguoi_dung` (`nguoi_dung_id`, `email`, `mat_khau`, `ho_ten`, `tuoi`, `anh_dai_dien`) VALUES
(4, 'kan123@gmail.com', '123456', 'Kan', 25, NULL),
(5, 'galayx@gmail.com', '123456', 'Kan', 25, NULL),
(6, 'galayxne@gmail.com', '$2b$10$CtmAiO97msFVZ8i/8eRbOeOLINYLzdF79FcBCVJVe3ad4XhnpnWW.', 'Kan', 25, NULL),
(7, 'galayxday@gmail.com', '$2b$10$AThc8ztfUSG30eS2HR7c0.yvpdgNS78uIuEvQszKPo5r1E6NYla2e', 'Kan', 25, NULL),
(8, 'galayx1602@gmail.com', '$2b$10$b59v8N0SY7s3dgDgjvJT5OTBjyBWP2YsbfqyooRh4W8QC5iogwTh6', 'Thien Ha', 18, NULL),
(9, 'galayx160298@gmail.com', '$2b$10$tPG1h5ZUXbLtztE3iZNpyuv6HrPKlx/9hiPNbrRFuoryRS6x2C2a.', 'Kan', 25, NULL),
(10, 'cr8@gmail.com', '$2b$10$e.ENnVz53LUSFT5Hhld2LOtW7v2uVMIbBYjL9QDNZ9l177T3HOc4S', 'Thien Ha Ne', 182, NULL),
(11, 'cr7siu@gmail.com', '$2b$10$crHjVHqRBlLQgX10s03AlOXBiwgAUM4unQ2Ede6QuFuiOFCqSa11m', 'cr7', 18, NULL);


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;