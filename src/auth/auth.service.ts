import { Injectable, HttpException } from '@nestjs/common';
import { PrismaClient, nguoi_dung } from '@prisma/client';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { CreateNguoidungDto } from 'src/nguoidung/dto/create-nguoidung.dto';

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    private configService: ConfigService,
  ) {}
  prisma = new PrismaClient();

  async logIn({ email, mat_khau }) {
    let checkNguoiDung = await this.prisma.nguoi_dung.findFirst({
      where: { email },
    });
    if (checkNguoiDung) {
      if (bcrypt.compareSync(mat_khau, checkNguoiDung.mat_khau)) {
        let token = this.jwtService.signAsync(
          { data: checkNguoiDung },
          { secret: this.configService.get('KEY'), expiresIn: '365d' },
        );
        return token;
      } else throw new HttpException('Mật khẩu không đúng!', 400);
    } else {
      throw new HttpException('Email không tồn tại!', 400);
    }
  }

  async signUp(createNguoiDungDto: CreateNguoidungDto) {
    try {
      let { email, mat_khau, ho_ten, tuoi } = createNguoiDungDto;
      let checkNguoiDung = await this.prisma.nguoi_dung.findMany({
        where: { email },
      });
      if (checkNguoiDung.length > 0) {
        return 'Email đã tồn tại';
      } else {
        let newNguoiDung = {
          email,
          mat_khau: bcrypt.hashSync(mat_khau, 10),
          ho_ten,
          tuoi,
        };
        await this.prisma.nguoi_dung.create({ data: newNguoiDung });
        return 'Đăng ký thành công';
      }
    } catch (err) {
      throw new HttpException(err, 500);
    }
  }
}
