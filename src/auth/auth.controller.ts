import { Controller, Post, Body, Get } from '@nestjs/common';
import { AuthService } from './auth.service';
import { ApiTags } from '@nestjs/swagger';
import { CreateNguoidungDto } from 'src/nguoidung/dto/create-nguoidung.dto';
@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}
  @Post('/login')
  logIn(@Body() body) {
    return this.authService.logIn(body);
  }
  @Post('/signup')
  signUp(@Body() createNguoiDung: CreateNguoidungDto) {
    return this.authService.signUp(createNguoiDung);
  }
}
