import { HttpException, Injectable } from '@nestjs/common';
import { CreateChitietDto } from './dto/create-chitiet.dto';
import { UpdateChitietDto } from './dto/update-chitiet.dto';
import { PrismaClient, binh_luan } from '@prisma/client';

@Injectable()
export class ChitietService {
  prisma = new PrismaClient();

  async getImageById(id: number) {
    try {
      let detailById = await this.prisma.hinh_anh.findFirst({
        where: { hinh_id: id },
        include: { nguoi_dung: true, binh_luan: true, luu_anh: true },
      });
      if (detailById) {
        let data = { ...detailById.nguoi_dung, mat_khau: '' };
        let result = { ...detailById, nguoi_dung: data };
        return result;
      } else {
        return 'Ảnh không tồn tại';
      }
    } catch {
      throw new HttpException('Lỗi BE', 500);
    }
  }
  async postComment(comment, id) {
    let data = { ...comment, ngay_binh_luan: new Date(), hinh_id: id };
    await this.prisma.binh_luan.create({ data: data });
    return 'Đã bình luận';
  }
}
