import { Module } from '@nestjs/common';
import { ChitietService } from './chitiet.service';
import { ChitietController } from './chitiet.controller';

@Module({
  controllers: [ChitietController],
  providers: [ChitietService]
})
export class ChitietModule {}
