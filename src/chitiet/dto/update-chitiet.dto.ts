import { PartialType } from '@nestjs/swagger';
import { CreateChitietDto } from './create-chitiet.dto';

export class UpdateChitietDto extends PartialType(CreateChitietDto) {}
