import { ApiProperty } from '@nestjs/swagger';
export class CreateChitietDto {
  @ApiProperty()
  ngay_binh_luan: Date;
  @ApiProperty()
  noi_dung: string;
  @ApiProperty()
  nguoi_dung_id: number;
  @ApiProperty()
  hinh_id: number;
}
