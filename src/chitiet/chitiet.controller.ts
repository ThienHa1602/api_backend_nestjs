import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { ChitietService } from './chitiet.service';
import { CreateChitietDto } from './dto/create-chitiet.dto';
import { UpdateChitietDto } from './dto/update-chitiet.dto';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { binh_luan } from '@prisma/client';
@ApiTags('Detail')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
@Controller('detail')
export class ChitietController {
  constructor(private readonly chitietService: ChitietService) {}

  @Get(':id')
  getImageById(@Param('id') id: string) {
    return this.chitietService.getImageById(+id);
  }
  @Post(':id')
  postComment(@Body() comment: CreateChitietDto, @Param('id') id: string) {
    return this.chitietService.postComment(comment, +id);
  }
}
