export class CreateNguoidungDto {
  email: string;
  mat_khau: string;
  ho_ten: string;
  tuoi: number;
}
