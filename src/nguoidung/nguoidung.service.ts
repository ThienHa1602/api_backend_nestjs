import { Injectable, HttpException } from '@nestjs/common';
import { CreateNguoidungDto } from './dto/create-nguoidung.dto';
import { UpdateNguoidungDto } from './dto/update-nguoidung.dto';
import { PrismaClient } from '@prisma/client';

@Injectable()
export class NguoidungService {
  prisma = new PrismaClient();

  async getUserInfo(id: number) {
    try {
      let userInfo = await this.prisma.nguoi_dung.findFirst({
        where: { nguoi_dung_id: id },
      });
      if (userInfo) {
        let data = { ...userInfo, mat_khau: '' };
        return data;
      } else {
        return 'Người dùng không tồn tại';
      }
    } catch {
      throw new HttpException('Lỗi BE', 500);
    }
  }

  async getUserSave(id: number) {
    try {
      let userInfo = await this.prisma.nguoi_dung.findFirst({
        where: { nguoi_dung_id: id },
        include: { luu_anh: true },
      });
      if (userInfo) {
        let data = { ...userInfo, mat_khau: '' };
        return data;
      } else {
        return 'Người dùng không tồn tại';
      }
    } catch {
      throw new HttpException('Lỗi BE', 500);
    }
  }
  async getUserCreate(id: number) {
    try {
      let userInfo = await this.prisma.nguoi_dung.findFirst({
        where: { nguoi_dung_id: id },
        include: { hinh_anh: true },
      });
      if (userInfo) {
        let data = { ...userInfo, mat_khau: '' };
        return data;
      } else {
        return 'Người dùng không tồn tại';
      }
    } catch {
      throw new HttpException('Lỗi BE', 500);
    }
  }
  async deleteImage(idUser: number, idImage: number) {
    try {
      await this.prisma.hinh_anh.delete({
        where: { nguoi_dung_id: idUser, hinh_id: idImage },
      });
      return 'Xóa ảnh thành công';
    } catch {
      throw new HttpException('Ảnh không tồn tại', 400);
    }
  }
  async postUpload(file: Express.Multer.File, id: number, body) {
    try {
      let { mo_ta } = body;
      let newImage = {
        ten_anh: file.filename,
        duong_dan: file.path,
        mo_ta: mo_ta,
        nguoi_dung_id: id,
      };
      await this.prisma.hinh_anh.create({ data: newImage });
      return 'Thêm ảnh thành công';
    } catch {
      throw new HttpException('Lỗi BE', 500);
    }
  }
  async updateUser(id: number, body) {
    try {
      let { ho_ten, tuoi } = body;
      let newUser = { ho_ten, tuoi };
      await this.prisma.nguoi_dung.update({
        data: newUser,
        where: { nguoi_dung_id: id },
      });
      return 'Cập nhật thông tin thành công';
    } catch {
      throw new HttpException('Người dùng không tồn tại', 400);
    }
  }
  async putAvarta(file: Express.Multer.File, id: number) {
    try {
      let newAvarta = {
        anh_dai_dien: file.path,
      };
      await this.prisma.nguoi_dung.update({
        data: newAvarta,
        where: { nguoi_dung_id: id },
      });
      return 'Cập nhật Avarta thành công';
    } catch (err) {
      throw new HttpException(err, 500);
    }
  }
}
