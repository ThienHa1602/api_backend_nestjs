import {
  Controller,
  Get,
  Put,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import { NguoidungService } from './nguoidung.service';
import { CreateNguoidungDto } from './dto/create-nguoidung.dto';
import { UpdateNguoidungDto } from './dto/update-nguoidung.dto';
import {
  ApiTags,
  ApiBearerAuth,
  ApiProperty,
  ApiConsumes,
  ApiBody,
} from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
class FileUploadDto {
  @ApiProperty({ type: 'string', format: 'binary' })
  file: any;
  @ApiProperty()
  mo_ta: string;
}
class FileUploadBodyDto {
  @ApiProperty()
  mo_ta: string;
}
@ApiTags('User')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
@Controller('user')
export class NguoidungController {
  constructor(private readonly nguoidungService: NguoidungService) {}

  @Get(':id')
  getUserInfo(@Param('id') id: string) {
    return this.nguoidungService.getUserInfo(+id);
  }

  @Get(':id/save')
  getUserSave(@Param('id') id: string) {
    return this.nguoidungService.getUserSave(+id);
  }
  @Get(':id/create')
  getUserCreate(@Param('id') id: string) {
    return this.nguoidungService.getUserCreate(+id);
  }
  @Delete(':idUser/create/:idImage')
  deleteImage(
    @Param('idUser') idUser: string,
    @Param('idImage') idImage: string,
  ) {
    return this.nguoidungService.deleteImage(+idUser, +idImage);
  }

  @Post(':id/upload')
  @ApiConsumes('multipart/form-data')
  @ApiBody({ type: FileUploadDto })
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: process.cwd() + '/public/img',
        filename: (req, file, callback) => {
          callback(null, new Date().getTime() + file.originalname);
        },
      }),
    }),
  )
  postUpload(
    @UploadedFile() file: Express.Multer.File,
    @Param('id') id: string,
    @Body() body: FileUploadDto,
  ) {
    return this.nguoidungService.postUpload(file, +id, body);
  }
  @Put(':id/profile')
  updateUser(@Param('id') id: string, @Body() body) {
    return this.nguoidungService.updateUser(+id, body);
  }
  @Put(':id/avarta')
  @ApiConsumes('multipart/form-data')
  @ApiBody({ type: FileUploadDto })
  @UseInterceptors(
    FileInterceptor('avarta', {
      storage: diskStorage({
        destination: process.cwd() + '/public/avarta',
        filename: (req, file, callback) => {
          callback(null, new Date().getTime() + file.originalname);
        },
      }),
    }),
  )
  putAvarta(
    @UploadedFile() file: Express.Multer.File,
    @Param('id') id: string,
  ) {
    return this.nguoidungService.putAvarta(file, +id);
  }
}
