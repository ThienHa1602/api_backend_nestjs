import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule } from '@nestjs/config';
import { JwtStrategy } from './strategy/jwt.strategy';
import { TrangchuModule } from './trangchu/trangchu.module';
import { ChitietModule } from './chitiet/chitiet.module';
import { NguoidungModule } from './nguoidung/nguoidung.module';

@Module({
  imports: [
    AuthModule,
    JwtModule.register({ global: true }),
    ConfigModule.forRoot({ isGlobal: true }),
    TrangchuModule,
    ChitietModule,
    NguoidungModule,
  ],
  controllers: [AppController],
  providers: [AppService, JwtStrategy],
})
export class AppModule {}
