import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { TrangchuService } from './trangchu.service';

import { AuthGuard } from '@nestjs/passport';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
@ApiTags('Home Page')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
@Controller('home-page')
export class TrangchuController {
  constructor(private readonly trangchuService: TrangchuService) {}
  @Get()
  getImages() {
    return this.trangchuService.getImages();
  }

  @Get('search/:search')
  getImageByName(@Param('search') search: string) {
    return this.trangchuService.getImageByName(search);
  }
}
