import { Module } from '@nestjs/common';
import { TrangchuService } from './trangchu.service';
import { TrangchuController } from './trangchu.controller';

@Module({
  controllers: [TrangchuController],
  providers: [TrangchuService]
})
export class TrangchuModule {}
