import { PartialType } from '@nestjs/swagger';
import { CreateTrangchuDto } from './create-trangchu.dto';

export class UpdateTrangchuDto extends PartialType(CreateTrangchuDto) {}
