import { HttpException, Injectable } from '@nestjs/common';

import { PrismaClient } from '@prisma/client';

@Injectable()
export class TrangchuService {
  prisma = new PrismaClient();

  async getImages() {
    let hinhAnh = await this.prisma.hinh_anh.findMany();
    return hinhAnh;
  }

  async getImageByName(search: string) {
    try {
      let hinhAnhTheoTen = await this.prisma.hinh_anh.findMany({
        where: { ten_anh: { contains: search } },
      });
      if (hinhAnhTheoTen.length > 0) {
        return hinhAnhTheoTen;
      } else {
        return 'Ảnh không tồn tại';
      }
    } catch (err) {
      throw new HttpException('Lỗi BE', 500);
    }
  }
}
